
$(document).ready(function () {

    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // link api to get course
    const gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
    // content type
    const gCONTENT_TYPE = "application/json";
    // declare datatable        
    const gCOL_TITLE = ["stt", "courseCode", "courseName", "price", "discountPrice", "duration", "level", "teacherName", "action"];
    const gSTT_COL = 0;
    const gCOURSE_CODE_COL = 1;
    const gCOURSE_NAME_COL = 2;
    const gPRICE_COL = 3;
    const gDISCOUNTED_PRICE_COL = 4;
    const gDURATION_COL = 5;
    const gLEVEL_COL = 6;
    const gTEACHER_COL = 7;
    const gACTION_COL = 8;
    // variable to save courses
    var gCourseDB = [];
    // declare variable to render STT
    var gStt = 1;
    var gCourseTable = $("#course-table").DataTable({
        columns: [
            { data: gCOL_TITLE[gSTT_COL] },
            { data: gCOL_TITLE[gCOURSE_CODE_COL] },
            { data: gCOL_TITLE[gCOURSE_NAME_COL] },
            { data: gCOL_TITLE[gPRICE_COL] },
            { data: gCOL_TITLE[gDISCOUNTED_PRICE_COL] },
            { data: gCOL_TITLE[gDURATION_COL] },
            { data: gCOL_TITLE[gLEVEL_COL] },
            { data: gCOL_TITLE[gTEACHER_COL] },
            { data: gCOL_TITLE[gACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gSTT_COL,
                render: function () {
                    return gStt++;
                }
            },
            {
                targets: gACTION_COL,
                defaultContent: `<i class="fa fa-edit text-primary edit-icon"></i> &nbsp; <i class="fas fa-trash-alt text-danger delete-icon"></i>`
            },
        ]
    });
    // declare variable to save course id of clicked row
    var gCurrentCourseId = null;
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    $(document).ready(function () {
        onPageLoading();
        // assign btn new course click event
        $("#btn-new-course").on("click", function () {
            onNewCourseBtnClick();
        });
        // assign btn insert course click event
        $("#btn-insert-course").on("click", function () {
            onInsertCourseBtnClick();
        });
        // assign icon edit course click event
        $("#course-table").on("click", ".edit-icon", function () {
            onEditCourseIconClick(this);
        });
        // assign update course btn click event
        $("#btn-update-course").on("click", function () {
            onUpdateCourseBtnClick();
        });
        // assign delete course icon click event
        $("#course-table").on("click", ".delete-icon", function () {
            onDeleteCourseIconClick(this);
        });
        // assign confirm delete btn click event
        $("#btn-delete-course-confirm").on("click", function () {
            onConfirmDeleteBtnClick();
        });
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict"
        // call api to get all course from sever
        callApiGetCourses();
        // load courses to table
        loadCourseToTable(gCourseDB);
    }
    // declare new course btn event
    function onNewCourseBtnClick() {
        "use strict"
        $("#modal-new-course").modal("show");
    }
    // declare insert course btn click event
    function onInsertCourseBtnClick() {
        "use strict"
        // 1. get data
        var vCourseData = {
            courseCode: "",
            courseName: '',
            price: '',
            discountPrice: '',
            duration: '',
            level: '',
            coverImage: '',
            teacherName: '',
            teacherPhoto: '',
            isPopular: '',
            isTrending: ''
        };
        getInsertNewCourse(vCourseData);
        console.log(vCourseData);
        // 2. validate data
        var vIsDataValid = validateCourseData(vCourseData);
        if (vIsDataValid) {
            // 3. call api and process data
            callApiCreateNewCourse(vCourseData);
        }
    }
    // declare edit course icon click
    function onEditCourseIconClick(paramEditIcon) {
        "use strict"
        $("#modal-edit-course").modal("show");
        getCourseId(paramEditIcon);
        callApiGetCourseById(gCurrentCourseId);
    }
    // declare update course btn click
    function onUpdateCourseBtnClick() {
        'use strict'
        // 1. get data 
        var vUpdateCourseData = {
            courseCode: '',
            courseName: '',
            price: '',
            discountPrice: '',
            duration: '',
            level: '',
            coverImage: '',
            teacherName: '',
            teacherPhoto: '',
            isPopular: '',
            isTrending: ''
        };
        getUpdateCourseData(vUpdateCourseData);
        // 2 validate data
        console.log(vUpdateCourseData);
        var vIsDataValid = validateCourseData(vUpdateCourseData);
        if (vIsDataValid) {
            // 3. process data and display
            callApiUpdateCourse(vUpdateCourseData);
        }

    }
    // declare delete course icon click
    function onDeleteCourseIconClick(paramDeleteIcon) {
        "use strict"
        $("#modal-delete-course-confirmation").modal("show");
        getCourseId(paramDeleteIcon);
    }
    // declare confirm delete btn click
    function onConfirmDeleteBtnClick() {
        "use strict"
        $.ajax({
            type: "DELETE",
            url: gBASE_URL + "/courses/" + gCurrentCourseId,
            success: function (response) {
                console.log(response);
                alert("Delete course successfully!");
                $("#modal-delete-course-confirmation").modal("hide"); 
                onPageLoading();
            },
            error: function (paramError) {
                alert("Delete unsuccessfully. Please try again later");
                console.log(paramError.status);
            }
        });
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // reset and hide create new modal
    function hideAndResetEditForm() {
        "use strict"
        $("#inp-edit-course-code").val('');
        $("#inp-edit-course-name").val('');
        $("#inp-edit-price").val('');
        $("#inp-edit-discounted-price").val('');
        $("#inp-edit-duration").val('');
        $("#select-edit-level").val(0);
        $("#inp-edit-cover-image").val('');
        $("#inp-edit-teacher-name").val('');
        $("#inp-edit-teacher-photo-link").val('');
        $("#select-edit-is-popular").val(-1);
        $("#select-edit-is-trending").val(-1);
        $("#modal-edit-course").modal("hide");
    }
    // call api to update course data to sever
    // input: object contain updated course data 
    function callApiUpdateCourse(paramCourseData) {
        "use strict"
        $.ajax({
            type: "PUT",
            url: gBASE_URL + "/courses/" + gCurrentCourseId,
            data: JSON.stringify(paramCourseData),
            dataType: "json",
            contentType: gCONTENT_TYPE,
            success: function (response) {
                console.log(response);
                alert("Update successfully");
                hideAndResetEditForm()
                onPageLoading();
            },
            error: function (paramError) {
                alert("update unsuccessfully. Please try again later")
                console.log(paramError.status);
            }
        });
    }
    // function get update course data from modal
    // input: object to save update data
    function getUpdateCourseData(paramCourseData) {
        "use strict"
        paramCourseData.courseCode = $.trim($("#inp-edit-course-code").val());
        paramCourseData.courseName = $.trim($("#inp-edit-course-name").val());
        paramCourseData.price = parseFloat($.trim($("#inp-edit-price").val()));
        paramCourseData.discountPrice = parseFloat($.trim($("#inp-edit-discounted-price").val()));
        paramCourseData.duration = $.trim($("#inp-edit-duration").val());
        paramCourseData.level = $.trim($("#select-edit-level").val());
        paramCourseData.coverImage = $.trim($("#inp-edit-cover-image").val());
        paramCourseData.teacherName = $.trim($("#inp-edit-teacher-name").val());
        paramCourseData.teacherPhoto = $.trim($("#inp-edit-teacher-photo-link").val());
        paramCourseData.isPopular = $("#select-edit-is-popular").val() == -1 ? -1 : 1 ? true : false;
        paramCourseData.isTrending = $("#select-edit-is-trending").val() == -1 ? -1 : 1 ? true : false;
    }
    // get Course from server by course id
    // input: course id
    function callApiGetCourseById(paramCourseID) {
        "use strict"
        $.ajax({
            type: "GET",
            url: gBASE_URL + "/courses/" + paramCourseID,
            dataType: "json",
            async: false,
            success: function (response) {
                console.log(response);
                loadCourseDetailToEditForm(response);
            },
            error: function (paramError) {
                alert("can get current course on server. Please try it later")
                console.log(paramError.status);
            }
        });
    }
    // load course detail to edit form
    // input: response object form sever after get course by id
    function loadCourseDetailToEditForm(paramCourseData) {
        "use strict"
        $("#inp-edit-course-code").val(paramCourseData.courseCode);
        $("#inp-edit-course-name").val(paramCourseData.courseName);
        $("#inp-edit-price").val(paramCourseData.price);
        $("#inp-edit-discounted-price").val(paramCourseData.discountPrice);
        $("#inp-edit-duration").val(paramCourseData.duration);
        $("#select-edit-level").val(paramCourseData.level);
        $("#inp-edit-cover-image").val(paramCourseData.coverImage);
        $("#inp-edit-teacher-name").val(paramCourseData.teacherName);
        $("#inp-edit-teacher-photo-link").val(paramCourseData.teacherPhoto);
        paramCourseData.isPopular == true ? $("#select-edit-is-popular").val("1") : false ? $("#select-edit-level").val("0") : $("#select-edit-level").val(-1);
        paramCourseData.isTrending == true ? $("#select-edit-is-trending").val("1") : false ? $("#select-edit-level").val("0") : $("#select-edit-level").val(-1);
    }
    // get course id of click row then save to gCurrentCourseId variable
    // input: current icon element
    function getCourseId(paramIcon) {
        "use strict"
        var vCurrentRow = $(paramIcon).parents("tr");
        var vCurrentRowData = gCourseTable.row(vCurrentRow).data();
        gCurrentCourseId = vCurrentRowData.id;
        // console.log(gCurrentCourseId);
    }
    // call api to create new course in server
    // input: valid data to create
    function callApiCreateNewCourse(paramCourseData) {
        'use strict'
        $.ajax({
            type: "POST",
            url: gBASE_URL + "/courses",
            data: JSON.stringify(paramCourseData),
            dataType: "json",
            contentType: gCONTENT_TYPE,
            async: false,
            success: function (response) {
                console.log(response);
                alert("Create new course successful");
                // reset form and hide modal
                hideAndResetCreateForm();
                onPageLoading();
            },
            error: function (paramError) {
                console.log(paramError.status);
            }
        });
    }
    // reset and hide create new modal
    function hideAndResetCreateForm() {
        "use strict"
        $("#inp-course-code").val('');
        $("#inp-course-name").val('');
        $("#inp-price").val('');
        $("#inp-discounted-price").val('');
        $("#inp-duration").val('');
        $("#select-level").val(0);
        $("#inp-cover-image").val('');
        $("#inp-teacher-name").val('');
        $("#inp-teacher-photo-link").val('');
        $("#select-is-popular").val(-1);
        $("#select-is-trending").val(-1);
        $("#modal-new-course").modal("hide");
    }
    // validate new course data
    function validateCourseData(paramCourseData) {
        "use strict"
        var vResult = true;
        if (paramCourseData.courseCode.length <= 10) {
            alert("Course code must have at least 10 character");
            vResult = false;
        } else if (paramCourseData.courseName.length <= 20) {
            alert("Course code must have at least 20 character");
            vResult = false;
        } else if (isNaN(paramCourseData.price) || paramCourseData.price <= 0) {
            alert("Price must be an integer greater than 0");
            vResult = false;
        } else if (isNaN(paramCourseData.discountPrice) || paramCourseData.discountPrice <= 0 || paramCourseData.discountPrice > paramCourseData.price) {
            alert("Discounted price must be an integer greater than 0 and less than price");
            vResult = false;
        } else if (paramCourseData.duration == "") {
            alert("Input course duration");
            vResult = false;
        } else if (paramCourseData.level == 0) {
            alert("select course level");
            vResult = false;
        } else if (paramCourseData.coverImage == "") {
            alert("Input cover image link");
            vResult = false;
        } else if (paramCourseData.teacherName == "") {
            alert("Input teacher name");
            vResult = false;
        } else if (paramCourseData.coverImage == "") {
            alert("Input cover image link");
            vResult = false;
        } else if (paramCourseData.teacherPhoto == "") {
            alert("Input teacher photo link");
            vResult = false;
        } else if (paramCourseData.isPopular == -1) {
            alert("select course popular");
            vResult = false;
        } else if (paramCourseData.isTrending == -1) {
            alert("select course trending");
            vResult = false;
        }
        return vResult;
    }
    // function get new course data from modal
    // input: object to save new data
    function getInsertNewCourse(paramCourseData) {
        "use strict"
        paramCourseData.courseCode = $.trim($("#inp-course-code").val());
        paramCourseData.courseName = $.trim($("#inp-course-name").val());
        paramCourseData.price = parseFloat($.trim($("#inp-price").val()));
        paramCourseData.discountPrice = parseFloat($.trim($("#inp-discounted-price").val()));
        paramCourseData.duration = $.trim($("#inp-duration").val());
        paramCourseData.level = $.trim($("#select-level").val());
        paramCourseData.coverImage = $.trim($("#inp-cover-image").val());
        paramCourseData.teacherName = $.trim($("#inp-teacher-name").val());
        paramCourseData.teacherPhoto = $.trim($("#inp-teacher-photo-link").val());
        paramCourseData.isPopular = $("#select-is-popular").val() == -1 ? -1 : 1 ? true : false;
        paramCourseData.isTrending = $("#select-is-trending").val() == -1 ? -1 : 1 ? true : false;
    }
    // function to add course to data table 
    function loadCourseToTable(paramCourseDb) {
        "use strict"
        gStt = 1;
        gCourseTable.clear();
        gCourseTable.rows.add(paramCourseDb);
        gCourseTable.draw();
    }

    // function call api to get all course from server
    function callApiGetCourses() {
        "use strict"
        $.ajax({
            type: "GET",
            url: gBASE_URL + "/courses",
            dataType: "json",
            async: false,
            success: function (response) {
                console.log(response);
                gCourseDB = response;
            }
        });
    }
});