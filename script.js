$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-css.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 9,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Intermediate",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 10,
                courseCode: "FE_UIUX_COURSE_211",
                courseName: "Thinkful UX/UI Design Bootcamp",
                price: 950,
                discountPrice: 700,
                duration: "5h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-uiux.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: false,
                isTrending: false
            },
            {
                id: 11,
                courseCode: "FE_WEB_REACRJS_210",
                courseName: "Front-End Web Development with ReactJs",
                price: 1100,
                discountPrice: 850,
                duration: "6h 20m",
                level: "Advanced",
                coverImage: "images/courses/course-reactjs.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 12,
                courseCode: "FE_WEB_BOOTSTRAP_101",
                courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
                price: 750,
                discountPrice: 600,
                duration: "3h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-bootstrap.png",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 14,
                courseCode: "FE_WEB_RUBYONRAILS_310",
                courseName: "The Complete Ruby on Rails Developer Course",
                price: 2050,
                discountPrice: 1450,
                duration: "8h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-rubyonrails.png",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            }
        ]
    }
    // popular course data base
    var gPopularCourseDB = null;
    // trending course data base
    var gTrendingCourseDB = null;
    // link api
    const gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict"
        callApiGetCourseDB();
        // filter popular courses from courses data base got from server and save to gPopularCourseDB variable
        gPopularCourseDB = gCoursesDB.filter(function (item) {
            return item.isPopular;
        });
        console.log(gPopularCourseDB);
        displayPopularCourses(gPopularCourseDB);
        // filer trending courses from courses data base got from sever and save to gTrendingCourseDB
        gTrendingCourseDB = gCoursesDB.filter(function (item) {
            return item.isTrending;
        });
        console.log(gTrendingCourseDB);
        // display trending course 
        displayTrendingCourse(gTrendingCourseDB);

    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    // display 4 trending course to div trending course 
    // input: array containing list of trending course
    function displayTrendingCourse(paramTrendingCourse) {
        "use strict"
        for (let bI = 0; bI < 4; bI++) {
            const bTrendingCourse = paramTrendingCourse[bI];
            // display course image
            $("#trending-div .img-course").eq(bI).attr({src:bTrendingCourse.coverImage, alt: bTrendingCourse.courseName});
            //display course name
            $("#trending-div h5").eq(bI).html(bTrendingCourse.courseName);
            //display course duration
            $("#trending-div .p-course-duration").eq(bI).html(`<i class="far fa-clock"></i> ` + bTrendingCourse.duration + `&nbsp; &nbsp;` + bTrendingCourse.level);
            //display course discounted price
            $("#trending-div b").eq(bI).html("$" + bTrendingCourse.discountPrice);
            //display course price
            $("#trending-div del").eq(bI).html("$" + bTrendingCourse.price);
            // display teacher image
            $("#trending-div .img-teacher").eq(bI).attr({src: bTrendingCourse.teacherPhoto, alt: bTrendingCourse.teacherName});
            //display teacher name
            $("#trending-div span").eq(bI).html(`&nbsp;` + bTrendingCourse.teacherName);
        }
    }
    // display 4 popular course to div popular
    // input: array containing list of popular course
    function displayPopularCourses(paramPopularCoursers) {
        "use strict"
        for (let bI = 0; bI < 4; bI++) {
            const bPopularCourse = paramPopularCoursers[bI];
            // display course image
            $("#most-popular-div .img-course").eq(bI).attr({src:bPopularCourse.coverImage, alt: bPopularCourse.courseName});
            //display course name
            $("#most-popular-div h5").eq(bI).html(bPopularCourse.courseName);
            //display course duration
            $("#most-popular-div .p-course-duration").eq(bI).html(`<i class="far fa-clock"></i> ` + bPopularCourse.duration + `&nbsp; &nbsp;` + bPopularCourse.level);
            //display course discounted price
            $("#most-popular-div b").eq(bI).html("$" + bPopularCourse.discountPrice);
            //display course price
            $("#most-popular-div del").eq(bI).html("$" + bPopularCourse.price);
            // display teacher image
            $("#most-popular-div .img-teacher").eq(bI).attr({src: bPopularCourse.teacherPhoto, alt: bPopularCourse.teacherName});
            //display teacher name
            $("#most-popular-div span").eq(bI).html(`&nbsp;` + bPopularCourse.teacherName);
        }
    }
    // call api to get all course from sever and save it to gCourseDB variable
    function callApiGetCourseDB() {
        "use strict"
        $.ajax({
            type: "GET",
            url: gBASE_URL + "/courses",
            dataType: "json",
            async: false,
            success: function (response) {
                console.log(response);
                gCoursesDB = response;
            }
        });
    }
});
